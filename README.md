# Cybertech2019_AtelierInkscape

Programme Processing pour des ateliers très courts d'initiation à la découpe vinyle. Génération de formes par assemblage de blocs à la souris, puis édition dans Inkscape pour conformer le fichier.

**Librairies**
Ce programme utilise :
- [L'adaptation de Box2D de daniel Shiffman](https://github.com/shiffman/Box2D-for-Processing/blob/master/LICENSE)
- [ControlP5, d'Andreas Schlegel](http://www.sojamo.de/libraries/controlP5/)
- [Toxiclibs, de Karsten Schmidt](https://bitbucket.org/postspectacular/toxiclibs/downloads/)
- Les librairies de base de Processing pour l'export SVG et PDF

**Dessin des formes, à l'aide de quadrilatères.**   
![Etape 1](images/Step1.PNG)

**Composition, dans un environnement pysique.**   
Les formes s'empilent, rebondissent, etc...   
![Etape 2](images/Step2.PNG)

**Export en SVG ou PDF, ouverture dans Inkscape**    
Les contours font apparaître la structure interne des formes. Il faut la simplifier à l'aide de l'outil "Union" des chemins.   
![Etape 3](images/Step3.PNG)
![Etape 4](images/Step4.PNG)
![Etape 5](images/Step5.PNG)

**Importation du résultat dans le logiciel de pilotage du plotter de découpe**   
![Etape 6](images/Step6.PNG)

**Découpe et échenillage**   
![Etape 7](images/Step7.jpg)
![Etape 8](images/Step8.jpg)