// The Nature of Code //<>// //<>//
// <http://www.shiffman.net/teaching/nature>
// Spring 2011
// Box2DProcessing example

class CustomShape {

  ArrayList<float[]> theQuads;
  // Array to check position of mouse
  ArrayList<Polygon2D> mousePoly;
  Body body;
  int ID;

  // Constructor
  CustomShape(ArrayList<float[]> _theQuads, int _ID) {
    // Add the box to the box2d world
    theQuads = new ArrayList<float[]>(_theQuads);
    mousePoly = new ArrayList<Polygon2D>();
    ID = _ID;
    makeBody();
  }
  /*
  // This function removes the particle from the box2d world
   void killBody() {
   box2d.destroyBody(body);
   }
   */
  /*
  // Is the particle ready for deletion?
   boolean done() {
   // Let's find the screen position of the particle
   Vec2 pos = box2d.getBodyPixelCoord(body);
   // Is it off the bottom of the screen?
   if (pos.y > height) {
   killBody();
   return true;
   }
   return false;
   }
   */
  // Drawing the box
  void display() {
    // We look at each body and get its screen position
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Get its angle of rotation
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    fill(29, 51, 90);
    noStroke();

    //TODO
    // Les polygon2D sont créés dans le pushMatrix et ne sont donc pas updatés en position.
    //Il faut récupérer leur position à l'écran
    mousePoly.clear();
    
    //Test pour union
    ArrayList<RPolygon> myUnion = new ArrayList<RPolygon>(); 
    
    //beginShape(QUADS);
    for (int i=0; i<theQuads.size(); i++)
    {
      
      //Polygon2D to be able to check the mouse
      Polygon2D poly=new Polygon2D();
      float[] myCoordinates = theQuads.get(i);
      
      //Test pour union
      RPoint[] points = new RPoint[4];
      RPolygon polygon;
      
      for (int j=0; j<8; j+=2)
      {
        Vec2 v = new Vec2(myCoordinates[j]*scale, myCoordinates[j+1]*scale);
        //vertex(v.x, v.y);
        poly.add(new Vec2D(screenX(v.x, v.y), screenY(v.x, v.y)));
        
        //Test pour union
        points[j/2] = new RPoint(v.x, v.y);
        
      }
      mousePoly.add(poly);
      polygon = new RPolygon(points);
      myUnion.add(polygon);
     
   
      
    }
    RPolygon polygonUnion = myUnion.get(0);
    for (int k = 0; k<myUnion.size(); k++)
    {
      if (k >0)
      {
        polygonUnion = polygonUnion.union(myUnion.get(k));
      }
    }
    RShape polygonUnionShape = polygonUnion.toShape();
    
    polygonUnionShape.draw();
    
    //endShape(CLOSE);
    
    popMatrix();
  }

  // This function adds the rectangle to the box2d world
  void makeBody() {

    // Define the body and make it from the shape
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(new Vec2((int)random(width/2-width/4,width/2-width/3 ), height/2 - (ID*200))));
    body = box2d.createBody(bd);

    for (int i=0; i<theQuads.size(); i++)
    {
      //We create each quad of the list

      // Define a polygon (this is what we use for a rectangle)
      PolygonShape sd = new PolygonShape();
      float[] myCoordinates = theQuads.get(i);
      Vec2[] vertices = new Vec2[4];

      for (int j=0; j<8; j+=2)
      {
        PVector myVector = new PVector(myCoordinates[j], myCoordinates[j+1]);
        vertices[j/2] = box2d.vectorPixelsToWorld(new Vec2(myVector.x*scale, myVector.y*scale));
      }

      sd.set(vertices, vertices.length);
      FixtureDef fd = new FixtureDef();
      fd.shape = sd;
      fd.density = 1;
      fd.friction = 0.6f;
      fd.restitution = 0.5f;
      body.createFixture(fd);
      body.createFixture(sd, 1.0);
    }
    // Give it some initial random velocity
    body.setLinearVelocity(new Vec2(random(-5, 5), (random(-5, 5) )));
    body.setAngularVelocity(random(-5, 5));
  }


  boolean contains(float x, float y)
  {
    boolean inside = false;
    for (int i= 0; i<mousePoly.size(); i++)
    {
      Polygon2D poly=mousePoly.get(i);
      // draw it for debug
      /*
      beginShape();
       stroke(0);
       noFill();
       for (int j = 0; j<poly.getNumVertices(); j++)
       {
       Vec2D myVert = poly.vertices.get(j);
       vertex(myVert.x,myVert.y); 
       }
       endShape(CLOSE);
       */
      //println(x, y, poly.getCentroid().x(), poly.getCentroid().y() );
      if  (poly.containsPoint(new Vec2D(x, y))) {
        inside = true;
      }
    }
    //println("mousePoly.size()= "+mousePoly.size()+" inside: "+inside);
    return inside;
  }
}
