import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import processing.pdf.*;
import java.util.Collections;
import java.util.Random;
import java.util.Arrays;
import controlP5.*;
import toxi.geom.*;
import processing.svg.*;

//Global Objects
ControlP5 cp5;
// A reference to our box2d world
Box2DProcessing box2d;

//PARAMS ////////////////////////////////////////////////////////////
// LetterDesign
float rows = 6;
float margin = 50;
int maxPolygons = 12;
int currentMaxPolygons = 6;

// Scale factor between the drawing and the letters on screen
float scale = 0.35;

// Debounce time for the mouse click/release
int debounce = 200;
////////////////////////////////////////////////////////////

//Storing everything
PVector[][] thePoints;
ArrayList<PVector> clickedPoints;
ArrayList<float[]> theQuads;
ArrayList<CustomShape> polygons;
float widthRow;

//Physics 
ArrayList<Boundary> boundaries;
ArrayList<Boundary> boundariesBottom;
Spring spring;

// Export
boolean savePDF = false;
boolean saveSVG = false;
boolean recording = false;

int state = 0;

long lastPressed = 0;

//////////////////////////////////////////:

PFont f;
ButtonBar b;
int barHeight = 20;

void setup() {

  // Size of window
  size(600, 700);
  smooth();
  ellipseMode (CENTER);

  f = createFont("DejaVu Sans Mono", 10);
  textFont(f);


  //Mesh
  //Store intersections points of the mesh
  thePoints = new PVector [(int)rows+1][(int)rows+1];
  clickedPoints = new ArrayList<PVector>();
  // Width of a column
  widthRow = (width-(margin*2))/rows;
  storePoints();

  // What is drawn by user
  theQuads = new ArrayList<float[]>() ;
  polygons = new ArrayList<CustomShape>();

  // Initialize box2d physics and create the world
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  // We are setting a custom gravity
  box2d.setGravity(0, -50);

  // Side and Bottom of our universe	
  boundaries = new ArrayList<Boundary>();
  boundariesBottom = new ArrayList<Boundary>();

  // Make the spring (it doesn't really get initialized until the mouse is clicked)
  spring = new Spring();

  // Add a bunch of fixed boundaries
  boundaries.add(new Boundary(-5, 0, 10, height*4));
  boundaries.add(new Boundary(width+5, 0, 10, height*4));
  createBoundariesBottom();

  //GUI
  cp5 = new ControlP5(this);
  b = cp5.addButtonBar("bar")
    .setPosition(0, height-barHeight)
    .setSize(width, barHeight)
    .addItems(split("Enregistrer la forme.Annuler la forme.Composer", "."))
    ;
}

//We store the points to check the position of the mouse.
void  storePoints()
{
  for (int i=0; i<rows+1; i++)
  {
    for (int j=0; j<rows+1; j++)
    {
      thePoints[i][j] = new PVector(margin + i*widthRow, margin+ widthRow*j);
    }
  }
}

void draw() {
  switch (state)
  {
    // User will draw some custom letters
  case 0:
    background(50);
    fill(0);
    rect(margin, margin, width-margin*2, width-margin*2); // same height as width

    // Draw Col & Rows
    noStroke();
    fill(75);
    for (int i=0; i<rows+1; i++)
    {
      for (int j=0; j<rows+1; j++)
      {
        PVector myVector =  thePoints[i][j];
        ellipse(myVector.x, myVector.y, 6, 6);
      }
    }
    stroke(75);
    for (int i=0; i<rows+1; i++)
    {
      line(margin + widthRow*i, margin, margin+ widthRow*i, margin+width-margin*2);
      line(margin, margin +widthRow*i, width-margin, margin+widthRow*i);
    }

    // If available, we store quads
    // if we have some quads
    if (clickedPoints.size()==4)
    {
      int j = 0;
      float[] myQuad = {clickedPoints.get(j).x, clickedPoints.get(j).y, clickedPoints.get(j+1).x, clickedPoints.get(j+1).y, clickedPoints.get(j+2).x, clickedPoints.get(j+2).y, clickedPoints.get(j+3).x, clickedPoints.get(j+3).y};
      theQuads.add(myQuad);
      clickedPoints.clear();
    }

    // Then, we draw the quads
    for (int j=0; j<theQuads.size(); j++)
    {
      fill(29, 51, 90);
      noStroke();
      float[] myQuad = theQuads.get(j);
      quad(myQuad[0], myQuad[1], myQuad[2], myQuad[3], myQuad[4], myQuad[5], myQuad[6], myQuad[7]);
    }

    // We draw clicked points
    for (int i=0; i<clickedPoints.size(); i++)
    {
      PVector myVector = clickedPoints.get(i);
      noStroke();
      fill(48, 83, 149);
      ellipse( myVector.x, myVector.y, 10, 10);
    }
    noStroke();
    drawMiniatures();
    checkMouse();

    break;

  case 1:
    // Arranging the created shapes 
    background(200);
    b.changeItem("Enregistrer la forme", "text", "Retour");
    b.changeItem("Annuler la forme", "text", "Export PDF");
    b.changeItem("Composer", "text", "Export SVG");


    // We must always step through time!
    box2d.step();

    // Always alert the spring to the new mouse location
    spring.update(mouseX, mouseY);

    if (savePDF == true) {
      beginRecord(PDF, "Cybertech" + frameCount + ".pdf");
    }

    if (saveSVG == true) {
      // Note that #### will be replaced with the frame number. Fancy!
      beginRecord(SVG, "Cybertech" + frameCount + ".svg");
    }

    // Display all the polygons
    for (CustomShape cs : polygons) {
      cs.display();
    }

    if (savePDF == true) {
      endRecord();
      println("PDF exporté");
      savePDF = false;
    }

    if (saveSVG == true) {
      endRecord();
      println("SVG exporté");
      saveSVG = false;
    }

    break;
  }
}

// When the mouse is released we're done with the spring
void mouseReleased() {
  spring.destroy();
}


void bar(int n) {
  switch (n)
  {
  case 0:
    if (state == 0)
    {
      // We check if we have enough points
      if (theQuads.size()>0 && polygons.size()<maxPolygons)
      {
        CustomShape myShape = new CustomShape(theQuads, polygons.size());
        polygons.add(myShape);
        clickedPoints.clear();
        theQuads.clear();
        if (polygons.size() > currentMaxPolygons && currentMaxPolygons <maxPolygons)currentMaxPolygons++;
      }
    } else if (state == 1)
    {
      b.clear();
      b.addItems(split("Enregistrer la forme.Annuler la forme.Composer", "."));
      state = 0;
    }
    break;

  case 1:
    if (state == 0)
    {
      clickedPoints.clear();
      theQuads.clear();
    } else if (state == 1)
    {
      savePDF = true;
    }

    break;

  case 2:
    if (state == 0)
    {
      state = 1;
    } else if (state == 1)
    {
      saveSVG = true;
    }
    break;
  }
}

void drawMiniatures()
{
  float sizeOfCell=  (width-margin*2)/currentMaxPolygons;
  //Let's help the user by displaying the shapes
  for (int i=0; i<currentMaxPolygons; i++)
  {
    pushMatrix();
    translate(margin+(i*sizeOfCell), margin+( width-margin*2)+25);
    fill(150);
    rect(2, 2, sizeOfCell-4, sizeOfCell-4);
    scale((sizeOfCell-4)/(width-margin*2)*0.85);
    fill(29, 51, 90);
    noStroke();
    if (i<polygons.size())
    {
      CustomShape myShape = polygons.get(i);
      for (int j=0; j<myShape.theQuads.size(); j++)
      {
        float[] myQuad = myShape.theQuads.get(j);
        quad(myQuad[0], myQuad[1], myQuad[2], myQuad[3], myQuad[4], myQuad[5], myQuad[6], myQuad[7]);
      }
    }
    scale(1/((sizeOfCell-4)/(width-margin*2)*0.85));
    fill(255);
    text(i+1+"/"+maxPolygons, 5, 13);
    popMatrix();
  }
}

void checkMouse()
{
  if (mouseY<height-barHeight)
  {
  // Kind of snapping for cursor, around the mesh
  for (int i=0; i<rows+1; i++)
  {
    for (int j=0; j<rows+1; j++)
    {
      PVector myVector =  thePoints[i][j];
      if (overCircle((int)myVector.x, (int) myVector.y, (int)widthRow/2))
      {
        noStroke();
        fill(48, 83, 149);
        ellipse((int)myVector.x, (int) myVector.y, 10, 10);
        if (mousePressed && millis()-lastPressed>debounce)
        {
          lastPressed = millis();
          boolean alreadyAdded = false;
          // we check if this particular point has not already been added
          for (int k = 0; k<clickedPoints.size(); k++)
          {
            PVector myCheckedPoint  = clickedPoints.get(k);
            if (myVector.x == myCheckedPoint.x && myVector.y == myCheckedPoint.y)
            {
              alreadyAdded = true;
            }
          }
          if (alreadyAdded == false)clickedPoints.add(myVector);
        }
      }
    }
  }
  }
}

boolean overCircle(int x, int y, int diameter) {
  float disX = x - mouseX;
  float disY = y - mouseY;
  if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
    return true;
  } else {
    return false;
  }
}

// When the mouse is pressed we. . .
void mousePressed() {
  if (state>0 && mouseY<height-barHeight)
  {
    for (int i = 0; i<polygons.size(); i++)
    {
      CustomShape myShape = polygons.get(i);
      // Check to see if the mouse was clicked on the box
      if (myShape.contains(mouseX, mouseY)) {
        // And if so, bind the mouse location to the box with a spring
        spring.bind(mouseX, mouseY, myShape);
        //selectedBox = theBlob;
      }
    }
  }
}

/*
//Could be usefull later, but not now
 void shuffleArray(String[] array) {
 
 // with code from WikiPedia; Fisher–Yates shuffle 
 //@ <a href="http://en.wikipedia.org/wiki/Fisher" target="_blank" rel="nofollow">http://en.wikipedia.org/wiki/Fisher</a>–Yates_shuffle
 
 Random rng = new Random();
 
 // i is the number of items remaining to be shuffled.
 for (int i = array.length; i > 1; i--) {
 
 // Pick a random element to swap with the i-th element.
 int j = rng.nextInt(i);  // 0 <= j <= i-1 (0-based array)
 
 // Swap array elements.
 String tmp = array[j];
 array[j] = array[i-1];
 array[i-1] = tmp;
 }
 }
 */

void createBoundariesBottom()
{
  // Création de boundaries pour la trappe
  int widthBoundary = width;
  int originX = width/2;
  int originY = height-13;
  boundariesBottom.add(new Boundary(originX, originY, widthBoundary, 25));
}
